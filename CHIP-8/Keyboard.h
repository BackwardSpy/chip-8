#pragma once

#include <map>
#include <SFML/Window.hpp>
#include "Types.h"

class Keyboard
{
private:
	std::map<sf::Keyboard::Key, byte> m_keys;
	bool m_states[16];

public:
	Keyboard();

	void press(sf::Keyboard::Key key);
	void release(sf::Keyboard::Key key);

	bool isPressed(byte keyID);

	bool anyKeyPressed(byte &keyID);
};