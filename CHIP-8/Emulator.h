#pragma once

#include <string>
#include "Types.h"
#include "Display.h"
#include "Keyboard.h"

class Emulator
{
public:
	byte V[16];
	u16 I;

	u16 PC() const { return m_programCounter; }
	Keyboard keyboard() const { return m_keyboard; }
	byte DT() const { return m_delayTimer; }
	byte ST() const { return m_soundTimer; }

	Emulator(const std::string &romFilename);

	void tick();
	u16 fetch();

	u16 fetchNoModify() const;

	bool isRunning() { return m_running; }
	void stop() { m_running = false; }
	void start() { m_running = true; }

	void doEvent(const sf::Event &event);
	void render(sf::RenderTarget &rt, const sf::Vector2f &origin);

	template<class T>
	T memRead(u16 addr) const;

	template<class T>
	void memWrite(u16 addr, T value);

private:
	//RAM Used by the Emulator
	byte m_RAM[0x1000];

	// The STACK :OOOOOOOOOOOOOO
	u16 m_stack[16];

	//Special purpose 8 bit registers auto decrimented by 60Hz when non-zero
	byte m_soundTimer;
	byte m_delayTimer;

	//Stores currently executing address
	u16 m_programCounter;

	//Stores current top of stack
	byte m_stackPointer;

	bool m_running;

	bool m_waitingForKey;
	byte m_keyWaitRegister;

	Display m_display;
	Keyboard m_keyboard;
};

template<class T>
T Emulator::memRead(u16 addr) const
{
	// Get a pointer to that section of RAM:
	const byte *head = &m_RAM[addr];

	// Read it back as type T (be careful with pointer casts like this, they can be incredibly fun if you get them wrong):
	return *(T *)head;
}

template<class T>
void Emulator::memWrite(u16 addr, T value)
{
	byte *head = &m_RAM[addr];
	*(T *)head = value;
}
