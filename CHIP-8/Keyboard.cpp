#include "Keyboard.h"

Keyboard::Keyboard()
{
	for (auto &state : m_states) state = false;

	m_keys[sf::Keyboard::Num1] = 0x1; // 1
	m_keys[sf::Keyboard::Num2] = 0x2; // 2
	m_keys[sf::Keyboard::Num3] = 0x3; // 3
	m_keys[sf::Keyboard::Num4] = 0xC; // C
		
	m_keys[sf::Keyboard::Q] = 0x4; // 4
	m_keys[sf::Keyboard::W] = 0x5; // 5
	m_keys[sf::Keyboard::E] = 0x6; // 6
	m_keys[sf::Keyboard::R] = 0xD; // D
		
	m_keys[sf::Keyboard::A] = 0x7; // 7
	m_keys[sf::Keyboard::S] = 0x8; // 8
	m_keys[sf::Keyboard::D] = 0x9; // 9
	m_keys[sf::Keyboard::F] = 0xE; // E
		
	m_keys[sf::Keyboard::Z] = 0xA; // A
	m_keys[sf::Keyboard::X] = 0x0; // 0
	m_keys[sf::Keyboard::C] = 0xB; // B
	m_keys[sf::Keyboard::V] = 0xF; // F
}

void Keyboard::press(sf::Keyboard::Key key)
{
	auto &it = m_keys.find(key);
	if (it == m_keys.end()) return;
	m_states[it->second] = true;
}

void Keyboard::release(sf::Keyboard::Key key)
{
	auto &it = m_keys.find(key);
	if (it == m_keys.end()) return;
	m_states[it->second] = false;
}

bool Keyboard::isPressed(byte keyID)
{
	return m_states[keyID];
}

bool Keyboard::anyKeyPressed(byte &keyID)
{
	for(byte key = 0; key < 16; key++)
	{
		if (m_states[key])
		{
			keyID = key;
			return true;
		}
	}

	return false;
}